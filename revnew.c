#include <stdio.h>
int main() {
    int n, p = 0, q;
    printf("Enter the integer: ");
    scanf("%d", &n);
    while (n != 0)
        {
        q = n % 10;
        p = p * 10 + q;
        n = n/10;
       }
    printf("Reversed number = %d", p);
    return 0;
}

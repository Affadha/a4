#include <stdio.h>
int main() {
    int n, h;
    printf("Enter an integer: ");
    scanf("%d", &n);
    for (h = 1; h <= 10; ++h) {
        printf("%d * %d = %d \n", n, h, n * h);
    }
    return 0;
}
